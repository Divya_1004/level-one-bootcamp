#include<stdio.h>
struct fract
{
	int num;
	int deno;
};
typedef struct fract fraction;
fraction input()
{
	fraction x;
	scanf("%d %d", &x.num, &x.deno);
	return x;
}

fraction simplify(fraction sum)
{
	int gcd=find_gcd(sum.num,sum.deno);
	sum.num=sum.num/gcd;
	sum.deno=sum.deno/gcd;
	return sum;
}
fraction compute_sum(fraction nm1,fraction nm2)
{
	fraction sum;
	sum.num=(nm1.num*nm2.deno)+(nm2.num*nm1.deno);
	sum.deno=(nm1.deno*nm2.deno);
	sum=simplify(sum);
	return sum;
}
int find_gcd(int a, int b)
{
	int  temp;
	while(a!=0)
	{
	temp=a;
	a=b%a;
	b=temp;
	}
return b;
}
void display_sum(fraction nm1, fraction nm2, fraction sum)
{
	printf("the sum of %d/%d and %d/%d is %d/%d", nm1.num, nm1.deno, nm2.num, nm2.deno, sum.num, sum.deno);
}
	int main()
{
    fraction f1,f2,sum;
    printf("Enter the value of numerator and denominator for first fraction:");
	f1=input("a/b");
	printf("Enter the value of numerator and denominator for second fraction:");
	f2=input("c/d");
	sum=compute_sum(f1,f2);
	display_sum(f1,f2,sum);
	return 0;
}